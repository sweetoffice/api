<?php

class ApiController extends \BaseController {

	/**
	 * Efetuar login
	 *
	 * @return Response
	 */
	public function login($cliente)
	{
		if($cliente == 'sweetoffice') return $this->sweetoffice();
		if($cliente == 'sweetstore') 	return $this->sweetstore();
		return false;
	}

	public function sweetoffice()
	{
		// Campo
		$userdata = array
		(
			'usuario_email'   => Input::get('usuario_email'),
			'password'  			=> Input::get('usuario_password')
		);
		if(is_null($userdata['password'])) return Response::json([ 'response' => 'Por favor, informe a senha', 'codigo' => '404' ]);
		// Select pelo email informado
		$verifica = UsuariosOffice::where('usuario_email', $userdata['usuario_email'])->first();
		// Verifica usuário
		if(is_null($verifica)) return Response::json([ 'response' => 'E-mail incorreto', 'codigo' => '404' ]);
		// Verifica senha
		$md5 			= md5( $userdata['usuario_email'].$userdata['password'] );
		$mdBanco 	= $verifica['password'];

		if($md5 != $mdBanco)	return Response::json([ 'response' => 'Senha incorreta', 'codigo' => '404' ]);
		// Retorna se não houver erro
		$del_token = OauthAccessTokens::where('id', $verifica['access_token'])->delete();
		$up_token = UsuariosOffice::where('usuario_email', $userdata['usuario_email'])->update(['access_token' => Input::get('access_token')]);
		$verifica = UsuariosOffice::where('usuario_email', $userdata['usuario_email'])->first();

		unset($verifica['password']);
		$verifica['access_token'] = Input::get('access_token');
		$retorno = Response::json([ 'response' => $verifica, 'codigo' => 200 ]);
		return $retorno;
	}

	public function sweetstore(){

	}

}
