<?php

class BancoController extends \BaseController {

protected $banco = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Banco $banco)
 {
	 $this->banco = $banco;
 }

	public function get_bancos()
	{
		$retorna = $this->banco->todos();
    return Response::json(['response' => $retorna]);
	}

}
