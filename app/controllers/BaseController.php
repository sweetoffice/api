<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	function inicial()
	{
		return Redirect::away('http://velrino.com.br/');
	}

	function tabela($tabela)
	{
		$verifica_tabela = Schema::getColumnListing($tabela);
		return $verifica_tabela;
	}

	// function teste($senha)
	// {
	// 	$senha = Hash::make($senha);
	// 	dd($senha);
	// 	// $verifica_tabela = Schema::getColumnListing($tabela);
	// 	// return $verifica_tabela;
	// }

	/**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function logout()
	{
		Auth::logout();
		$mensagem = "Deslogado";
		$codigo = 200;
		$retorno = Response::json(([ 'response' => $mensagem ]), $codigo);

		return $retorno;
	}

	function get_token()
	{
		// return apache_get_modules();
		return csrf_token();
	}


}
