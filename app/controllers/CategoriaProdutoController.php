<?php

class CategoriaProdutoController extends \BaseController {

protected $categoriaProduto = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(CategoriaProduto $categoriaProduto)
 {
	 $this->categoriaProduto = $categoriaProduto;
 }

	public function get_categoriaProduto($id)
	{

		$retorna = $this->categoriaProduto->get_produtoCategoria($id);
    return Response::json(['response' => $retorna]);
	}

}
