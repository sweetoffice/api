<?php

class CidadeController extends \BaseController {

protected $cidade = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Cidade $cidade)
 {
	 $this->cidade = $cidade;
 }

  public function get_cidadesEstados($uf)
	{
		$retorna = $this->cidade->cidadeEstado($uf);
    return Response::json(['response' => $retorna]);
	}

}
