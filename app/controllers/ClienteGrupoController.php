<?php

class ClienteGrupoController extends \BaseController {

protected $clienteGrupo = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(ClienteGrupo $clienteGrupo)
 {
	 $this->clienteGrupo = $clienteGrupo;
 }

	public function get_clienteGrupo()
	{
		$retorna = $this->clienteGrupo->todos();
    return Response::json(['response' => $retorna]);
	}

}
