<?php

class ClienteTipoController extends \BaseController {

protected $clienteTipo = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(ClienteTipo $clienteTipo)
 {
	 $this->clienteTipo = $clienteTipo;
 }

	public function get_clienteTipo()
	{
		$retorna = $this->clienteTipo->todos();
    return Response::json(['response' => $retorna]);
	}

}
