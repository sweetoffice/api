<?php

class EmpresaController extends \BaseController {

protected $empresa = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Empresa $empresa)
 {
	 $this->setor = $empresa;
 }

	public function get_empresas($status = 'ativo')
	{
		$retorna = $this->setor->todos($status);
    return Response::json(['response' => $retorna]);
	}

}
