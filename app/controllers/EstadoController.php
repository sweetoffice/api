<?php

class EstadoController extends \BaseController {

protected $estado = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Estado $estado)
 {
	 $this->estado = $estado;
 }

	public function get_estados($status = 'ativo')
	{
		$retorna = $this->estado->todos($status);
    return Response::json(['response' => $retorna]);
	}

}
