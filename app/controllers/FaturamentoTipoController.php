<?php

class FaturamentoTipoController extends \BaseController {

protected $faturamentoTipo = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(FaturamentoTipo $faturamentoTipo)
 {
	 $this->faturamentoTipo = $faturamentoTipo;
 }

	public function get_faturamentoTipo()
	{
		$retorna = $this->faturamentoTipo->todos();
    return Response::json(['response' => $retorna]);
	}

}
