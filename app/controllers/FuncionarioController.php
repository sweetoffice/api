<?php

class FuncionarioController extends \BaseController {

protected $funcionario = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Funcionario $funcionario)
 {
	 $this->funcionario = $funcionario;
 }

	public function get_funcionarios($status = 'ativo')
	{
    $get = Funcionario::find($id);
    if(is_null($get)) return NULL;
    return Response::json(['response' => $response]);
	}

  public function get_funcionario($id)
	{
		$get = Funcionario::find($id);
    if(is_null($get)) return NULL;
    $response['funcionario'] = $get;
    $get->cargos;
    $get->funcionarioDocumento;
    $get->funcionarioEndereco;
    $get->funcionarioCaracteristicas;
    $get->funcionarioBeneficario;
    $get->funcionarioRegistro;
    $get->funcionarioTelefone;
    $get->funcionarioEstrangeiro;
    $get->funcionarioFerias;
    $get->funcionarioTransporte;
    $get->funcionarioContribuicao;
    $get->funcionarioEscolaridade;
    return Response::json(['response' => $response]);
	}

  public function get_funcionario_documento($campo, $valor)
  {
    $c_FuncionarioDocumento = new FuncionarioDocumento;
    $retorna = $c_FuncionarioDocumento->documentoFuncionario($campo, $valor);
    $retorna = (!is_null($retorna)) ? true : false;
    return Response::json(['response' => $retorna]);
  }

}
