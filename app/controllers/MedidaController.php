<?php

class MedidaController extends \BaseController {

protected $medida = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(ProdutoMedida $medida)
 {
	 $this->medida = $medida;
 }

	public function get_medidas()
	{
		$retorna = $this->medida->todos();
    return Response::json(['response' => $retorna]);
	}

}
