<?php

class MoedaController extends \BaseController {

protected $moeda = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Moeda $moeda)
 {
	 $this->moeda = $moeda;
 }

	public function get_moedas($status = 'ativo')
	{
		$retorna = $this->moeda->todos($status);
    return Response::json(['response' => $retorna]);
	}

}
