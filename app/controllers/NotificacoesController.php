<?php

class NotificacoesController extends \BaseController {

	protected $notificacoes = null;
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
	 function __construct(Notificacoes $notificacoes)
	 {
		 $this->notificacoes = $notificacoes;
	 }

		public function get_notificacoes($id)
		{
			$retorna = $this->notificacoes->selectNotificacoes($id);
	    return Response::json(['response' => $retorna]);
		}

}
