<?php

class PaisController extends \BaseController {

protected $pais = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Pais $pais)
 {
	 $this->pais = $pais;
 }

	public function get_paises($status = 'ativo')
	{
		$retorna = $this->pais->todos($status);
    return Response::json(['response' => $retorna]);
	}

}
