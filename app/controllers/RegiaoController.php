<?php

class RegiaoController extends \BaseController {

protected $regiao = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Regiao $regiao)
 {
	 $this->regiao = $regiao;
 }

	public function get_regiao()
	{
		$retorna = $this->regiao->todos();
    return Response::json(['response' => $retorna]);
	}

}
