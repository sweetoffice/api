<?php

class SetorController extends \BaseController {

protected $setor = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Setor $setor)
 {
	 $this->setor = $setor;
 }

	public function get_setores($status = 'ativo')
	{
		$retorna = $this->setor->todos($status);
    return Response::json(['response' => $retorna]);
	}

  public function get_cargos($id)
	{
		$retorna = Setor::find($id);
    $retorna = (!is_null($retorna)) ? $retorna : false;
    return Response::json(['response' => $retorna->cargo]);
	}

}
