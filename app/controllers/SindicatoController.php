<?php

class SindicatoController extends \BaseController {

protected $sindicato = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Sindicato $sindicato)
 {
	 $this->sindicato = $sindicato;
 }

	public function get_sindicatos()
	{
		$retorna = $this->sindicato->todos();
    return Response::json(['response' => $retorna]);
	}

}
