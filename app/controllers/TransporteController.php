<?php

class TransporteController extends \BaseController {

protected $transporte = null;
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
 function __construct(Transporte $transporte)
 {
	 $this->transporte = $transporte;
 }

	public function get_transportesCidadeEstado($estado, $cidade)
	{
		$retorna = $this->transporte->getTransportesCidadeEstado($estado, $cidade);
    return Response::json(['response' => $retorna]);
	}

}
