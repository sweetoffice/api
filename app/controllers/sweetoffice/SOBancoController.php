<?php

class SOBancoController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_banco()
	{
		$inputs = (object) Input::all();
		// INPUTS
		$banco = (isset($inputs->banco)) ? $inputs->banco : null ;
		$usuario =  $inputs->usuario;
		$createBanco = new Banco;
		$createBanco = $createBanco->createBanco( $banco );

		$notificacoes = new Notificacoes();
		$mensagem = $createBanco->banco_nome.' inserido com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Banco';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}


		function update_banco( $id )
		{
			$inputs = (object) Input::all();
			// INPUTS
			$usuario =  $inputs->usuario;
			$banco =  $inputs->banco;

			$updateBanco = new Banco;
			$updateBanco = $updateBanco->updateBanco( $id, $banco );

			$notificacoes = new Notificacoes();
			$mensagem = $banco["banco_nome"].' atualizado com sucesso';
			// Notificacao
			$notificacao['emissor_id'] 	= $usuario['usuario_id'];
			$notificacao['receptor_id'] = $usuario['usuario_id'];
			$notificacao['tipo'] 				= 'Banco';
			$notificacao['subtipo'] 		= 'Atualizar';
			$notificacao['descricao'] 	= $mensagem ;
			$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

			$retorno = Response::json(([ 'response' => $mensagem ]));
			return $retorno;
		}

		function delete_banco( $id )
		{
			$inputs = (object) Input::all();
			// INPUTS
			$usuario =  $inputs->usuario;

			$deleteBanco = new Banco;
			$deleteBanco = $deleteBanco->deleteBanco( $id );

			$notificacoes = new Notificacoes();
			$mensagem = $deleteBanco->banco_nome.' deletado com sucesso';
			// Notificacao
			$notificacao['emissor_id'] 	= $usuario['usuario_id'];
			$notificacao['receptor_id'] = $usuario['usuario_id'];
			$notificacao['tipo'] 				= 'Banco';
			$notificacao['subtipo'] 		= 'Deletar';
			$notificacao['descricao'] 	= $mensagem ;
			$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

			$retorno = Response::json(([ 'response' => $mensagem ]));
			return $retorno;
		}


}
