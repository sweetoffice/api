<?php

class SOBensController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_bem()
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$bem = new Bens();
		$notificacoes = new Notificacoes();

		$createBem = $bem->createBem( $inputs->bem );

		$mensagem = 'Item '.$createBem->bens_nome.' inserido com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Bem';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem, 'redireciona' => $createBem->bens_id ]));
		return $retorno;
	}

	function update_bem($id)
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$bem = new Bens();
		$notificacoes = new Notificacoes();

		$updateBem = $bem->updateBem( $id, $inputs->bem );

		$mensagem = 'Item '.$updateBem->bens_nome.' atualizado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Bem';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function delete_bem($id)
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$bem = new Bens();
		$notificacoes = new Notificacoes();

		$deleteBem = $bem->deleteBens( $id );

		$mensagem = 'Item '.$deleteBem->bens_nome.' deletado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Bem';
		$notificacao['subtipo'] 		= 'Excluir';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function get_bens()
	{
		$bem = new Bens();
		$bem = $bem->todos();
		$retorno = Response::json(([ 'response' => $bem ]));
		return $retorno;
	}

	function get_bem($id)
	{
		$bem = new Bens();
		$bem = $bem->selectBem($id);
		$retorno = Response::json(([ 'response' => $bem ]));
		return $retorno;
	}

}
