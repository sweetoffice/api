<?php

class SOCargoController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_cargo()
	{
		$inputs = (object) Input::all();
		$cargo = (isset($inputs->cargo)) ? $inputs->cargo : null ;
		$usuario =  $inputs->usuario;
		$createCargo = new Cargo;
		$createCargo = $createCargo->createCargo( $cargo );

		$notificacoes = new Notificacoes();
		$mensagem = $createCargo->cargo_nome.' inserido na Base de dados ';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Cargo';
		$notificacao['subtipo'] 		= 'Cadastro';	$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
