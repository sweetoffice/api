<?php

class SOCategoriaController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_categoria()
	{
		$inputs = (object) Input::all();
		// INPUTS
		$categoria = (isset($inputs->categoria)) ? $inputs->categoria : null ;
		$usuario =  $inputs->usuario;

		$createCategoria = new Categoria;
		$createCategoria = $createCategoria->createCategoria( $categoria );

		$notificacoes = new Notificacoes();
		$mensagem = $createCategoria->categoria_nome.' inserido com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Categoria';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function update_categoria( $id )
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario =  $inputs->usuario;
		$categoria =  $inputs->categoria;

		$updateCategoria = new Categoria;
		$updateCategoria = $updateCategoria->updateCategoria( $id, $categoria );

		$notificacoes = new Notificacoes();
		$mensagem = $categoria["categoria_nome"].' atualizado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Categoria';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function delete_categoria( $id )
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario =  $inputs->usuario;

		$deleteCategoria = new Categoria;
		$deleteCategoria = $deleteCategoria->deleteCategoria( $id );

		$notificacoes = new Notificacoes();
		$mensagem = $deleteCategoria->categoria_nome.' deletado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Categoria';
		$notificacao['subtipo'] 		= 'Deletar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}




}
