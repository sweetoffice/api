<?php

class SOClienteController extends \BaseController {

  /*
	| Clientes
	*/
	function get_clientes()
	{
		$c_cliente = new Cliente();
		$get = $c_cliente->todos();
		$retorno = Response::json(([ 'response' => $get ]));
		return $retorno;
	}
	/*
	| Capturar cliente
	*/
	function get_cliente($id)
 	{
		$get = Cliente::find($id);
	  if(is_null($get) OR $get['cliente_status'] == "excluido") return NULL;
	  $response['fornecedor'] = $get;
	  $response['endereco'] = ClienteEndereco::whereClienteIdFk($id)->get();
	  return Response::json(['response' => $response]);
 	}
	/*
	| Criar cliente
	*/
	function create_cliente()
	{
		$inputs = (object) Input::all();
		$c_cliente = new Cliente();
		$usuario = $inputs->usuario;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$banco = (isset($inputs->banco)) ? $inputs->banco : null ;
		$notificacoes = new Notificacoes();
		$createCliente = $c_cliente->createCliente( $inputs->cliente );
		$cliente_id = $createCliente->cliente_id;
		if(!is_null($endereco))
		{
			foreach ($endereco as $key => $value)
			{
				if( !empty($value['endereco_cep']) AND !empty($value['endereco_rua']) )
				{
					$c_endereco = new ClienteEndereco();
					$c_endereco->createClienteEndereco( $cliente_id, $endereco[$key] );
				}
			}
		}
		$mensagem = 'Cliente '.$createCliente->cliente_nome_fantasia.' inserido com sucesso';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Cliente';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem , 'redireciona' => $createCliente->cliente_id]));
		return $retorno;
	}
	/*
	| Editar cliente
	*/
	function update_cliente($id)
	{
		$inputs = (object) Input::all();
		$c_cliente = new Cliente();
		$usuario = $inputs->usuario;
		$cliente = $inputs->cliente;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$updateCliente = $c_cliente->updateCliente( $id, $cliente );
		if(!is_null($endereco))
		{
		  foreach ($endereco as $key => $value)
		  {
		    $c_endereco = new ClienteEndereco();
		    if(!empty($value['endereco_id']))
		    {
		      $getFuncionario = $c_endereco::where('endereco_id', $value['endereco_id'])->update($endereco[$key]);
		    }
		    if(empty($value['endereco_id']) AND !empty($value['endereco_cep'])  AND !empty($value['endereco_rua']) )
		    {
		      $c_endereco->createClienteEndereco( $id, $endereco[$key] );
		    }
		  }
		}
		$mensagem = 'Cliente '.$updateCliente->cliente_nome_fantasia.' atualizado com sucesso';
		// Notificacao
		$notificacoes = new Notificacoes();
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Cliente';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}
	/*
	| Deletar cliente
	*/
	function delete_cliente($id)
	{
		$inputs = (object) Input::all();
		$c_cliente = new Cliente();
		$usuario = $inputs->usuario;

		$deleteCliente = $c_cliente->deleteCliente( $id );

		$mensagem = 'Cliente '.$deleteCliente->cliente_nome_fantasia.' deletado com sucesso';

		$notificacoes = new Notificacoes();
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Cliente';
		$notificacao['subtipo'] 		= 'Deletar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
