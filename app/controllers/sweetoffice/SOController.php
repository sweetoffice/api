<?php

class SOController extends \BaseController {

		protected $parametro = null;
		/**
		 * Capturar na base de dados
		 *
		 * @return Response
		 */
		public function get_database($parametro)
		{
			if (Request::isJson())
			{
				$parametro = urldecode($parametro);
			  $parametro = explode('&', $parametro) ? explode('&', $parametro) : false ;
			  foreach ($parametro as $key)
			  {
			    $indexes = explode('=', $key);
			    $valores = explode('+', $indexes[1]);
			    $array[$indexes[0]] = $indexes[1];
			  }
			  $parametros = (object) $array;
		    // CARACTERES
		      $mais = '+'; $comercial = '&'; $igual = '=';
		      // CONDIÇÂO
		      $table 		= 	isset($parametros->table) ? $parametros->table 		: false ;
		      $column 	= 	isset($parametros->column) ? $parametros->column 	: false ;
		      $ordem 		= 	isset($parametros->order) ? $parametros->order 		: false ;
		      $where 		= 	isset($parametros->where) ? $parametros->where 		: false ;
		      $limit		= 	isset($parametros->limit) ? $parametros->limit 		: false ;

		      $verifica_tabela = Schema::getColumnListing($table);
		      if($verifica_tabela)
					{
	        $query = DB::table($table);
	        // Limite de resultados
	        if ( $limit ) $query->limit($limit);
	        // Selec colunas
	        if ( $column )
	        {
	          $selects = explode($mais, $parametros->column);
	          $query->select($selects);
	        }
	        // // OrderBy(Coluna, Valor)
	        if ( $ordem )
	        {
	          $ordens = explode($mais, $ordem);
	          foreach ($ordens as $value)
	          {
	            $col = explode($mais, $value);
	            $val = explode(',', $col[0]);
	            $ord[$val[0]] = $val[1];
	            $query->orderBy($val[0], $val[1]);
	          }
	        }
	        // Where(Coluna, Valor)
	        if ( $where )
	        {
	          $where = explode($mais, $where);
	          foreach ($where as $value)
	          {
	            $col = explode($mais, $value);
	            $val = explode(',', $col[0]);
	            $batman[$val[0]] = $val[1];
	          }
	          $query->where($batman);
	        }
					$query->where($table.'_status', '!=', 'excluido' );

	        $result = $query->get();
	        // RESULTADO
					$result = ($result) ? $result : '';
					return Response::json(['response' => $result]);
					}
		    }
			}

			public function get_quantidade($table, $coluna = null)
			{
				if (!Request::isJson()) return Redirect::to('/');
				$query = DB::table($table);
				$query->where($coluna.'_status', '!=', 'excluido' );
				$result = $query->get();
				$result = ($result) ? $result : '';
				return Response::json(['response' => $result]);
			}

}
