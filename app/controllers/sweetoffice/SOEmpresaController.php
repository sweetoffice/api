<?php

class SOEmpresaController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_empresa()
	{
		$inputs = (object) Input::all();
		// INPUTS
		$empresa = (isset($inputs->empresa)) ? $inputs->empresa : null ;
		$usuario =  $inputs->usuario;

		$createEmpresa = new Empresa;
		$createEmpresa = $createEmpresa->createEmpresa( $empresa );

		$notificacoes = new Notificacoes();
		$mensagem = $createEmpresa->empresa_nome.' inserido com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Empresa';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function update_empresa( $id )
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario =  $inputs->usuario;
		$empresa =  $inputs->empresa;

		$updateEmpresa = new Empresa;
		$updateEmpresa = $updateEmpresa->updateEmpresa( $id, $empresa );

		$notificacoes = new Notificacoes();
		$mensagem = $empresa["empresa_nome"].' atualizado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Empresa';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function delete_empresa( $id )
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario =  $inputs->usuario;

		$deleteEmpresa = new Empresa;
		$deleteEmpresa = $deleteEmpresa->deleteEmpresa( $id );

		$notificacoes = new Notificacoes();
		$mensagem = $deleteEmpresa->empresa_nome.' deletado com sucesso';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Empresa';
		$notificacao['subtipo'] 		= 'Deletar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}




}
