<?php

class SOFornecedorController extends \BaseController {

  /*
	| Fornecedor
	*/
	function get_fornecedores()
	{
		$c_fornecedor = new Fornecedor();
		$get = $c_fornecedor->todos();
		$retorno = Response::json(([ 'response' => $get ]));
		return $retorno;
	}
	/*
	| Capturar fornecedor
	*/
	function get_fornecedor($id)
 	{
		$get = Fornecedor::find($id);
		if(is_null($get) OR $get['fornecedor_status'] == "excluido") return NULL;
		$response['fornecedor'] = $get;
		$response['endereco'] = FornecedorEndereco::whereFornecedorIdFk($id)->get();
		$response['banco'] = FornecedorBanco::whereFornecedorIdFk($id)->get();
		return Response::json(['response' => $response]);
 	}
	/*
	| Criar fornecedor
	*/
	function create_fornecedor()
	{
		$inputs = (object) Input::all();
		$c_fornecedor = new Fornecedor();
		$usuario = $inputs->usuario;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$banco = (isset($inputs->banco)) ? $inputs->banco : null ;
		$notificacoes = new Notificacoes();
		$createFornecedor = $c_fornecedor->createFornecedor( $inputs->fornecedor );
		$fornecedor_id = $createFornecedor->fornecedor_id;
		if(!is_null($banco))
		{
			foreach ($banco as $key => $value)
			{
				$c_banco = new FornecedorBanco();
				$c_banco->createBanco( $fornecedor_id, $banco[$key] );
			}
		}
		if(!is_null($endereco))
		{
			foreach ($endereco as $key => $value)
			{
				$c_banco = new FornecedorEndereco();
				$c_banco->createFornecedorEndereco( $fornecedor_id, $endereco[$key] );
			}
		}

		$mensagem = 'Fornecedor '.$createFornecedor->fornecedor_nome_fantasia.' inserido com sucesso';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Fornecedor';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem , 'redireciona' => $fornecedor_id]));
		return $retorno;
	}
	/*
	| Editar fornecedor
	*/
	function update_fornecedor($id)
	{
		$inputs = (object) Input::all();
		$c_fornecedor = new Fornecedor();
		$usuario = $inputs->usuario;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$banco = (isset($inputs->banco)) ? $inputs->banco : null ;
		$notificacoes = new Notificacoes();
		$updateFornecedor = $c_fornecedor->updateFornecedor( $id, $inputs->fornecedor );

		if(!is_null($banco))
		{
			foreach ($banco as $key => $value)
			{
				$c_banco = new FornecedorBanco();
				if(!empty($value['fornecedor_banco_id']))
				{
					$getFuncionario = $c_banco::where('fornecedor_banco_id', $value['fornecedor_banco_id'])->update($banco[$key]);
				}
				if(empty($value['fornecedor_banco_id']) AND !empty($value['banco_id_fk']))
				{
					$c_banco->createBanco( $id, $banco[$key] );
				}
			}
		}
		if(!is_null($endereco))
		{
			foreach ($endereco as $key => $value)
			{
				$c_endereco = new FornecedorEndereco();
				if(!empty($value['fornecedor_endereco_id']))
				{
					$getFuncionario = $c_endereco::where('fornecedor_endereco_id', $value['fornecedor_endereco_id'])->update($endereco[$key]);
				}
				if(empty($value['fornecedor_endereco_id']) AND !empty($value['fornecedor_cep']) AND !empty($value['fornecedor_rua']))
				{
					$c_endereco->createFornecedorEndereco( $id, $endereco[$key] );
				}
			}
		}

		$mensagem = 'Fornecedor '.$updateFornecedor->fornecedor_nome_fantasia.' atualizado com sucesso';
		// Notificacao
		$notificacoes = new Notificacoes();
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Fornecedor';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}
	/*
	| Deletar fornecedor
	*/
	function delete_fornecedor($id)
	{
		$inputs = (object) Input::all();
		$c_fornecedor = new Fornecedor();
		$usuario = $inputs->usuario;

		$deleteFornecedor = $c_fornecedor->deleteFornecedor( $id );

		$mensagem = 'Fornecedor '.$deleteFornecedor->fornecedor_nome_fantasia.' deletado com sucesso';

		$notificacoes = new Notificacoes();
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Fornecedor';
		$notificacao['subtipo'] 		= 'Deletar';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
