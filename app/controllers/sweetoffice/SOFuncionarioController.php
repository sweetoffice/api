<?php

class SOFuncionarioController extends \BaseController {

	/*
	| Criar
	*/
	function create_funcionario()
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario = (isset($inputs->usuario)) ? $inputs->usuario : null ;
		$funcionario = (isset($inputs->funcionario)) ? $inputs->funcionario : null ;
		$transporte = (isset($inputs->transporte)) ? $inputs->transporte : null ;
		$empresa = (isset($inputs->empresa)) ? $inputs->empresa : null ;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$documento = (isset($inputs->documento)) ? $inputs->documento : null ;
		$beneficiario = (isset($inputs->beneficiario)) ? $inputs->beneficiario : null ;
		$telefone = (isset($inputs->telefone)) ? $inputs->telefone : null ;
		$caracteristicas = (isset($inputs->caracteristicas)) ? $inputs->caracteristicas : null ;
		$estrangeiro = (isset($inputs->estrangeiro)) ? $inputs->estrangeiro : null ;
		$escolaridade = (isset($inputs->escolaridade)) ? $inputs->escolaridade : null ;
		// Funcionario
		if (is_null($usuario) OR is_null($funcionario)) return false;
		$c_funcionario = new Funcionario();
		$c_funcionario->createFuncionario($funcionario);
		$f_id 	= $c_funcionario->funcionario_id;
		$f_nome = $c_funcionario->funcionario_nome;
		// Funcionario Escolaridade
		if(!is_null($escolaridade))
		{
			foreach ($escolaridade as $key => $value)
			{
				$c_funcionarioEscolaridade = new FuncionarioEscolaridade();
				$c_funcionarioEscolaridade->creteFuncionarioEscolaridade( $f_id, $escolaridade[$key] );
			}
		}
		if(!is_null($transporte))
		{
			foreach ($transporte as $key => $value)
			{
				$c_funcionarioTransporte = new FuncionarioTransporte();
				$c_funcionarioTransporte->createTransporte( $f_id, $transporte[$key] );
			}
		}
		// Funcionario Beneficiário
		if(!is_null($beneficiario))
		{
			foreach ($beneficiario as $key => $value)
			{
				$c_funcionarioDocumento = new FuncionarioBeneficario();
				$c_funcionarioDocumento->createFuncionarioBeneficario( $f_id, $beneficiario[$key]);
			}
		}
		// Funcionario Telefone
		// Funcionario Telefone
		if(!is_null($telefone))
		{
			foreach ($telefone as $key => $value)
			{
				$c_funcionarioTelefone = new FuncionarioTelefone();
				$c_funcionarioTelefone->createFuncionarioTelefone( $f_id, $telefone[$key] );
			}
		}
		// Funcionario Documento
		if(!is_null($documento))
		{
			$c_funcionarioDocumento = new FuncionarioDocumento();
			$c_funcionarioDocumento->createFuncionarioDocumento( $f_id, $documento);
		}
		// Funcionario Endereço
		if(!is_null($endereco))
		{
			$c_funcionarioEndereco = new FuncionarioEndereco();
			$c_funcionarioEndereco->creteFuncionarioEndereco( $f_id, $endereco);
		}
		// Funcionario Registro
		if(!is_null($empresa))
		{
			$c_funcionarioRegistro = new FuncionarioRegistro();
			$c_funcionarioRegistro->createFuncionarioRegistro( $f_id, $empresa);
		}
		// Funcionario Caracteristicas
		if(!is_null($caracteristicas))
		{
			$c_funcionarioCaracteristicas = new FuncionarioCaracteristicas();
			$c_funcionarioCaracteristicas->createFuncionarioCaracteristicas( $f_id, $caracteristicas);
		}
		// Funcionario Estrangeiro
		if(!is_null($estrangeiro))
		{
			$c_funcionarioEstrangeiro = new FuncionarioEstrangeiro();
			$c_funcionarioEstrangeiro->createFuncionarioEstrangeiro( $f_id, $estrangeiro);
		}

		$notificacoes = new Notificacoes();
		$mensagem = 'Funcionário '.$f_nome.' inserido na banco de dados';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Funcionario';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem , 'redireciona' => $f_id ]));
		return $retorno;
	}
	/*
	| Atualizar
	*/
	function update_funcionario($id)
	{
		$inputs = (object) Input::all();
		// INPUTS
		$usuario = (isset($inputs->usuario)) ? $inputs->usuario : null ;
		$funcionario = (isset($inputs->funcionario)) ? $inputs->funcionario : null ;
		$transporte = (isset($inputs->transporte)) ? $inputs->transporte : null ;
		$empresa = (isset($inputs->empresa)) ? $inputs->empresa : null ;
		$endereco = (isset($inputs->endereco)) ? $inputs->endereco : null ;
		$documento = (isset($inputs->documento)) ? $inputs->documento : null ;
		$beneficiario = (isset($inputs->beneficiario)) ? $inputs->beneficiario : null ;
		$telefone = (isset($inputs->telefone)) ? $inputs->telefone : null ;
		$caracteristicas = (isset($inputs->caracteristicas)) ? $inputs->caracteristicas : null ;
		$estrangeiro = (isset($inputs->estrangeiro)) ? $inputs->estrangeiro : null ;
		$escolaridade = (isset($inputs->escolaridade)) ? $inputs->escolaridade : null ;

		// Funcionario
		if (is_null($usuario) OR is_null($funcionario)) return false;
		$c_funcionario = new Funcionario();
		// Verifica se o usuário existe
		$v_funcionario = $c_funcionario::find($id);
		if (is_null($v_funcionario)) return Response::json(([ 'response' => "Funcionário $id não existe" ]));
		$c_funcionario->updateFuncionario($id, $funcionario);
		$f_id 	= $id;
		$f_nome = $funcionario['funcionario_nome'];

		if(!is_null($transporte))
		{
			foreach ($transporte as $key => $value)
			{
				// if(empty($transporte[$key])) return false;
				$c_funcionarioTransporte = new FuncionarioTransporte();
				if(!empty($value['transporte_id']))
				{
					$getFuncionario = $c_funcionarioTransporte::where('transporte_id', $value['transporte_id'])->update($transporte[$key]);
				}
				if(empty($value['transporte_id']))
				{
					$c_funcionarioTransporte->createTransporte( $id, $transporte[$key]);
				}
			}
		}
		// Funcionario Beneficiário
		if(!is_null($escolaridade))
		{
		  foreach ($escolaridade as $key => $value)
		  {
		    $c_funcionarioEscolaridade = new FuncionarioEscolaridade();
		    if(!empty($value['escolaridade_id']))
		    {
		      $getFuncionario = FuncionarioEscolaridade::where('escolaridade_id', $value['escolaridade_id'])->update($escolaridade[$key]);
		    }
		    if(empty($value['escolaridade_id']) AND !empty($value['escolaridade_curso']))
		    {
		      $c_funcionarioEscolaridade->creteFuncionarioEscolaridade( $id, $escolaridade[$key] );
		    }
		  }
		}
		// Funcionario Beneficiário
		if(!is_null($beneficiario))
		{
			foreach ($beneficiario as $key => $value)
			{
				$c_funcionarioDocumento = new FuncionarioBeneficario();
				if(!empty($value['beneficiario_id']))
				{
					$getFuncionario = FuncionarioBeneficario::where('beneficiario_id', $value['beneficiario_id'])->update($beneficiario[$key]);
				}
				if(empty($value['beneficiario_id']) AND !empty($value['beneficiario_nome']))
				{
					$c_funcionarioDocumento->createFuncionarioBeneficario( $id, $beneficiario[$key] );
				}
			}
		}
		// Funcionario Beneficiário
		if(!is_null($beneficiario))
		{
			foreach ($beneficiario as $key => $value)
			{
				$c_funcionarioDocumento = new FuncionarioBeneficario();
				if(!empty($value['beneficiario_id']))
				{
					$getFuncionario = FuncionarioBeneficario::where('beneficiario_id', $value['beneficiario_id'])->update($beneficiario[$key]);
				}
				if(empty($value['beneficiario_id']) AND !empty($value['beneficiario_nome']))
				{
					$c_funcionarioDocumento->createFuncionarioBeneficario( $id, $beneficiario[$key] );
				}
			}
		}
		// Funcionario Telefone
		if(!is_null($telefone))
		{
			foreach ($telefone as $key => $value)
			{
				$c_funcionarioTelefone = new FuncionarioTelefone();
				if(!empty($value['telefone_id']))
				{
					$c_funcionarioTelefone = FuncionarioTelefone::where('telefone_id', $value['telefone_id'])->update($telefone[$key]);
				}
				if(empty($value['telefone_id']))
				{
					$c_funcionarioTelefone->createFuncionarioTelefone( $id, $telefone[$key] );
				}
			}
		}
		// Funcionario Documento
		if(!is_null($documento))
		{
			$c_funcionarioDocumento = FuncionarioDocumento::where('funcionario_id_fk', $id)->update($documento);
		}
		// Funcionario Endereço
		if(!is_null($endereco))
		{
			$c_funcionarioEndereco = FuncionarioEndereco::where('funcionario_id_fk', $id)->update($endereco);
		}
		// Funcionario Registro
		if(!is_null($empresa))
		{
			$c_funcionarioRegistro = FuncionarioRegistro::where('funcionario_id_fk', $id)->update($empresa);
		}
		// Funcionario Caracteristicas
		if(!is_null($caracteristicas))
		{
			$c_funcionarioCaracteristicas = FuncionarioCaracteristicas::where('funcionario_id_fk', $id)->update($caracteristicas);
		}
		// Funcionario Estrangeiro
		if(!is_null($estrangeiro))
		{
			$c_funcionarioEstrangeiro = FuncionarioEstrangeiro::where('funcionario_id_fk', $id)->update($estrangeiro);
		}

		$notificacoes = new Notificacoes();
		$mensagem = 'Funcionário '.$f_nome.' atualizado no banco de dados';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Funcionario';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function delete_funcionario($id)
	{
	  $inputs = (object) Input::all();
	  $usuario =  $inputs->usuario;
		$retorno = Response::json(([ 'response' => $usuario ]));
	  $produto = new Funcionario();
	  $notificacoes = new Notificacoes();
	  $deleteFuncionario = $produto->deleteFuncionario( $id );

	  if(is_null($deleteFuncionario)) return Response::json((['response' => 'Funcionario '.$id.' não existe']));

	  $mensagem = 'Funcionário '.$deleteFuncionario->funcionario_nome.' deletado ';
	  // // Notificacao
	  $notificacao['emissor_id'] 	= $usuario['usuario_id'];
	  $notificacao['receptor_id'] = $usuario['usuario_id'];
	  $notificacao['tipo'] 				= 'Funcionario';
	  $notificacao['subtipo'] 		= 'Excluir';
	  $notificacao['descricao'] 	= $mensagem ;
	  $createNotificacao = $notificacoes->criar_notificacao( $notificacao );

	  $retorno = Response::json(([ 'response' => $mensagem ]));
	  return $retorno;
	}

}
