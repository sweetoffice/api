<?php

class SONotificacoesController extends \BaseController {

	protected $notificacoes = null;
		/**
		 * Display a listing of the resource.
		 *
		 * @return Response
		 */
		 function __construct(Notificacoes $notificacoes)
		 {
			 $this->notificacoes = $notificacoes;
		 }

		function get_notificacao($user, $id)
 		{
 			$retorna = $this->notificacoes->selectNotificacao( $user, $id);
 	    return Response::json(['response' => $retorna]);
 		}

		function get_notificacoes($id)
		{
			$retorna = $this->notificacoes->selectNotificacoes($id);
	    return Response::json(['response' => $retorna]);
		}

		function update_notificacoes($id)
		{
			$retorna = $this->notificacoes->updateNotificacoes($id);
	    return Response::json(['response' => 'Visualizado']);
		}
}
