<?php

class SOProdutoController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_produto()
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$produto = new Produto();
		$c_produto = new CategoriaProduto();
		$notificacoes = new Notificacoes();

		$createProduto = $produto->createProduto( $inputs->produto );
		if(isset($inputs->produto_id_categoria ))
		{
			foreach ($inputs->produto_id_categoria as $key => $value)
			{
				$categoriaProduto = $c_produto->createCategoriaProduto( $value, $createProduto->produto_id );
			}
		}
		$mensagem = 'Produto '.$createProduto->produto_nome.' inserido com sucesso';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Produto';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem, 'redireciona' => $createProduto->produto_id ]));
		return $retorno;
	}

	function get_produto($id)
	{
		$produto = new Produto();
		$produto = $produto->selectProduto($id);
		$retorno = Response::json(([ 'response' => $produto ]));
		return $retorno;
	}

	function update_produto($id)
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$produto = new Produto();
		$c_produto = new CategoriaProduto();
		$notificacoes = new Notificacoes();

		$updateProduto = $produto->updateProduto( $id, $inputs->produto );

		$mensagem = 'Produto '.$updateProduto->produto_nome.' atualizado com sucesso';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Produto';
		$notificacao['subtipo'] 		= 'Atualizar';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

	function delete_produto($id)
	{
		$inputs = (object) Input::all();
		$usuario =  $inputs->usuario;
		$produto = new Produto();
		$notificacoes = new Notificacoes();

		$deleteProduto = $produto->deleteProduto( $id );
		if(is_null($deleteProduto)) return Response::json((['response' => 'Produto '.$id.' não existe']));

		$mensagem = 'Produto '.$deleteProduto->produto_nome.' deletado ';
		// // Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Produto';
		$notificacao['subtipo'] 		= 'Excluir';
		$notificacao['descricao'] 	= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
