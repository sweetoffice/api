<?php

class SOSetorController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_setor()
	{
		$inputs = (object) Input::all();
		$setor = (isset($inputs->setor)) ? $inputs->setor : null ;
		$usuario =  $inputs->usuario;
		$createSetor = new Setor;
		$createSetor = $createSetor->createSetor( $setor );

		$notificacoes = new Notificacoes();
		$mensagem = $createSetor->setor_nome.' inserido na Base de dados ';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Setor';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
