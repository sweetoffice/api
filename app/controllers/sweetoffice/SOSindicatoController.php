<?php

class SOSindicatoController extends \BaseController {

  /**
	 * Deslogar da API
	 *
	 * @return Response
	 */
	function create_sindicato()
	{
		$inputs = (object) Input::all();
		// INPUTS
		$sindicato  = (isset($inputs->sindicato )) ? $inputs->sindicato  : null ;
		$usuario =  $inputs->usuario;
		$createSindicato  = new Sindicato ;

		$createSindicato  = $createSindicato ->createSindicato( $sindicato );

		$notificacoes = new Notificacoes();
		$mensagem = $createSindicato ->sindicato_nome.' inserido no banco de dados ;) ';
		// Notificacao
		$notificacao['emissor_id'] 	= $usuario['usuario_id'];
		$notificacao['receptor_id'] = $usuario['usuario_id'];
		$notificacao['tipo'] 				= 'Sindicato ';
		$notificacao['subtipo'] 		= 'Cadastro';
		$notificacao['descricao'] 		= $mensagem ;
		$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

		$retorno = Response::json(([ 'response' => $mensagem ]));
		return $retorno;
	}

}
