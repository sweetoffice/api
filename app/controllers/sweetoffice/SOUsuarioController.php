<?php

class SOUsuarioController extends \BaseController {

		function create_usuario()
		{
			$inputs = (object) Input::all();
			// INPUTS
			$usuarioOffice 		= (isset($inputs->uoffice)) ? $inputs->uoffice : null ;
			$usuarioDireitos 	= (isset($inputs->udireitos)) ? $inputs->udireitos : null ;
			$usuario 					=  $inputs->usuario;
			$createUsuario = new UsuariosOffice;
			$createUsuario = $createUsuario->createUsuariosOffice( $usuarioOffice );

			$usuarioDireitos['usuario_id'] = $createUsuario->usuario_id;
			$nome = $createUsuario->usuario_nome;
			$createUsuario = new UsuariosModulos;
			$createUsuario = $createUsuario->createUsuariosModulos( $usuarioDireitos );

			$notificacoes = new Notificacoes();
			$mensagem = 'Usuário '. $nome.' cadastrado com sucesso';
			// Notificacao
			$notificacao['emissor_id'] 	= $usuario['usuario_id'];
			$notificacao['receptor_id'] = $usuario['usuario_id'];
			$notificacao['tipo'] 				= 'Usuario';
			$notificacao['subtipo'] 		= 'Cadastro';
			$notificacao['descricao'] 	= $mensagem ;
			$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

			$retorno = Response::json(([ 'response' => $mensagem ]));
			return $retorno;
		}

		function update_usuario($id)
		{
			$inputs = (object) Input::all();
			// INPUTS
			$usuarioOffice 		= (isset($inputs->uoffice)) ? $inputs->uoffice : null ;
			$usuarioDireitos 	= (isset($inputs->udireitos)) ? $inputs->udireitos : null ;
			$usuario 					=  $inputs->usuario;
			$nome = $usuarioOffice['usuario_nome'];

			$updateUsuario = new UsuariosOffice;
			$updateUsuario = $updateUsuario->updateUsuariosOffice( $id, $usuarioOffice );

			$createUsuario = new UsuariosModulos;
			$createUsuario = $createUsuario->updateUsuariosModulos( $id, $usuarioDireitos );

			$notificacoes = new Notificacoes();
			$mensagem = 'Usuário '. $nome.' atualizado com sucesso';
			// Notificacao
			$notificacao['emissor_id'] 	= $usuario['usuario_id'];
			$notificacao['receptor_id'] = $usuario['usuario_id'];
			$notificacao['tipo'] 				= 'Usuario';
			$notificacao['subtipo'] 		= 'Cadastro';
			$notificacao['descricao'] 	= $mensagem ;
			$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

			$retorno = Response::json(([ 'response' => $mensagem ]));
			return $retorno;
		}

		function delete_usuario($id)
		{
			$inputs = (object) Input::all();
			// INPUTS
			$usuario 					=  $inputs->usuario;

			$deleteUsuario = new UsuariosOffice;
			$deleteUsuario = $deleteUsuario->deleteUsuariosOffice( $id );
			$nome = $deleteUsuario['usuario_nome'];

			$deleteUsuarios = new UsuariosModulos;
			$deleteUsuarios = $deleteUsuarios->deleteUsuariosModulos( $id );

			$notificacoes = new Notificacoes();
			$mensagem = 'Usuário '. $nome.' deletado com sucesso';
			// Notificacao
			$notificacao['emissor_id'] 	= $usuario['usuario_id'];
			$notificacao['receptor_id'] = $usuario['usuario_id'];
			$notificacao['tipo'] 				= 'Usuario';
			$notificacao['subtipo'] 		= 'Cadastro';
			$notificacao['descricao'] 	= $mensagem ;
			$createNotificacao = $notificacoes->criar_notificacao( $notificacao );

			$retorno = Response::json(([ 'response' => $mensagem ]));
			return $retorno;
		}

	 	function usuario_modulos()
	 	{
	 		// Variáveis
			$_acesso = '_acesso';
			$_direitos = '_direitos';
			$codigo = 200;

		 	$meus_modulos = DB::table('usuarios_modulos')->where('usuario_id', Input::get('id'))->first();
			if($meus_modulos)
			{
				foreach ($meus_modulos as $coluna => $modulo)
				{
					// Acessos
					if (strpos($coluna, $_acesso))
					{
						if ( $modulo == 'sim' AND !is_null($modulo) )
						{
							$acesso[$coluna] = $modulo;
						}
					}
					// Direitos
					if (strpos($coluna, $_direitos))
					{
						if ( $modulo != "0,0,0,0" AND !is_null($modulo) )
						{
							$direito[$coluna] = $modulo;
						}
					}

				}

				$response['direitos'] 	= (isset($direito)) ? $direito 	: false ;
				$codigo = 200;
			}

			if(!$response)
			{
				$response = 'Usuário não encontrado';
				$codigo = 400;
			}

			$retorno = Response::json(([ 'response' => $response ]), $codigo);
			return $retorno;
	 	}
}
