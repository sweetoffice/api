<?php

/**
 * Eloquent class to describe the cidade table
 *
 * automatically generated by ModelGenerator.php
 */
class Cidade extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'cidade';

    protected $hidden = ['cidade_status'];

    public $primaryKey = 'cidade_id';

    public $timestamps = false;

    protected $fillable = array('cidade_estado', 'cidade_uf', 'cidade_nome');

    public function transporte()
    {
        return $this->hasMany('Transporte', 'cidade_id_fk', 'cidade_id');
    }

    public function cidadeEstado($uf, $ordem = 'ASC')
    {
      return self::whereCidadeUf($uf)->orderBy('cidade_nome', $ordem)->get();
    }
}
