<?php

/**
 * Eloquent class to describe the cliente_tipo table
 *
 * automatically generated by ModelGenerator.php
 */
class ClienteTipo extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'cliente_tipo';

    public $primaryKey = 'tipo_cliente_id';

    public $timestamps = false;

    protected $hidden = array('tipo_cliente_status');

    protected $fillable = array('tipo_cliente_nome');

    public function cliente()
    {
        return $this->hasMany('Cliente', 'cliente_tipo_id_fk', 'tipo_cliente_id');
    }

    public function todos()
    {
      return self::orderBy('tipo_cliente_nome', 'ASC')->get();
    }

    public function createClienteTipo($inputs)
    {
      $verifica = array_diff_key(array_flip($this->fillable), $inputs);
      if(count($verifica) != count($this->fillable))
      {
        $salvar = $this->fill($inputs);
        $salvar = $this->save();
      }
      return $this;
    }

}
