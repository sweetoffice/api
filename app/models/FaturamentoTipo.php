<?php

/**
 * Eloquent class to describe the faturamento_tipo table
 *
 * automatically generated by ModelGenerator.php
 */
class FaturamentoTipo extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'faturamento_tipo';

    public $primaryKey = 'faturamento_tipo_id';

    public $timestamps = false;

    protected $hidden = array('faturamento_tipo_status');

    protected $fillable = array('faturamento_tipo_nome');

    public function cliente()
    {
        return $this->hasMany('Cliente', 'faturamento_tipo_id_fk', 'faturamento_tipo_id');
    }

    public function fornecedor()
    {
        return $this->hasMany('Fornecedor', 'faturamento_id_fk', 'faturamento_tipo_id');
    }

    public function pedidoVenda()
    {
        return $this->hasMany('PedidoVenda', 'faturamento_tipo_id_fk', 'faturamento_tipo_id');
    }

    public function todos()
    {
      return self::orderBy('faturamento_tipo_nome', 'ASC')->get();
    }

    public function createFaturamentoTipo($inputs)
    {
      $verifica = array_diff_key(array_flip($this->fillable), $inputs);
      if(count($verifica) != count($this->fillable))
      {
        $salvar = $this->fill($inputs);
        $salvar = $this->save();
      }
      return $this;
    }
}
