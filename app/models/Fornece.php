<?php

/**
 * Eloquent class to describe the fornece table
 *
 * automatically generated by ModelGenerator.php
 */
class Fornece extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'fornece';

    public $primaryKey = 'fornece_id';

    public $timestamps = false;

    protected $fillable = array();

    public function fornecedor()
    {
        return $this->belongsTo('Fornecedor', 'fornecedor_id_fk', 'fornecedor_id');
    }

    public function produto()
    {
        return $this->belongsTo('Produto', 'produto_id_fk', 'produto_id');
    }

}

