<?php

/**
 * Eloquent class to describe the funcionario table
 *
 * automatically generated by ModelGenerator.php
 */
class Funcionario extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'funcionario';

    public $primaryKey = 'funcionario_id';

    public $timestamps = false;

    protected $fillable = array('funcionario_codigo','funcionario_nome', 'funcionario_escolaridade',
        'funcionario_estado_civil', 'funcionario_nacionalidade', 'funcionario_nascimento', 'funcionario_pais',
        'funcionario_mae', 'funcionario_conjulgue', 'funcionario_admissao', 'funcionario_desligamento',
        'funcionario_observacoes', 'funcionario_sexo', 'funcionario_status', 'funcionario_sn', 'funcionario_sobrenome',
        'funcionario_email_pes', 'funcionario_email_emp', 'funcionario_convenio', 'funcionario_combustivel_km',
        'funcionario_combustivel_valor', 'funcionario_alimentacao');

    public function funcionarioBeneficario()
    {
        return $this->hasMany('FuncionarioBeneficario', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioCaracteristicas()
    {
        return $this->hasMany('FuncionarioCaracteristicas', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioContribuicao()
    {
        return $this->hasMany('FuncionarioContribuicao', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioDocumento()
    {
        return $this->hasMany('FuncionarioDocumento', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioEndereco()
    {
        return $this->hasMany('FuncionarioEndereco', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioEscolaridade()
    {
        return $this->hasMany('FuncionarioEscolaridade', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioEstrangeiro()
    {
        return $this->hasMany('FuncionarioEstrangeiro', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioFerias()
    {
        return $this->hasMany('FuncionarioFerias', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioParalisacao()
    {
        return $this->hasMany('FuncionarioParalisacao', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioRegistro()
    {
        return $this->hasMany('FuncionarioRegistro', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioTelefone()
    {
        return $this->hasMany('FuncionarioTelefone', 'funcionario_id_fk', 'funcionario_id');
    }

    public function funcionarioTransporte()
    {
        return $this->hasMany('FuncionarioTransporte', 'funcionario_id_fk', 'funcionario_id');
    }

    public function usuariosOffice()
    {
        return $this->hasMany('UsuariosOffice', 'funcionario_id_fk', 'funcionario_id');
    }

    public function createFuncionario($funcionario)
    {
      $verifica = array_diff_key(array_flip($this->fillable), $funcionario);
      if(count($verifica) != count($this->fillable))
      {
        $salvar = $this->fill($funcionario);
        $salvar = $this->save();
      }
      return $this;
    }

    public function updateFuncionario($id, $funcionario)
    {
      $verifica = array_diff_key(array_flip($this->fillable), $funcionario);
      if(count($verifica) != count($this->fillable))
      {
        $getFuncionario = Funcionario::find($id);
        $getFunc = $getFuncionario->fill($funcionario);
        $salvar = $getFunc->save();
      }
      return $getFuncionario;
    }

    function deleteFuncionario($id)
    {
      $getFuncionario = Funcionario::find($id);
      if(is_null($getFuncionario)) return NULL;
      $getFuncionario->funcionario_status = 'excluido';
      $getFuncionario->save();
      return $getFuncionario;
    }

}
