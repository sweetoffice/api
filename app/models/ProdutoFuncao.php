<?php

/**
 * Eloquent class to describe the produto_funcao table
 *
 * automatically generated by ModelGenerator.php
 */
class ProdutoFuncao extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'produto_funcao';

    public $primaryKey = 'produto_id_funcao';

    public $timestamps = false;

    protected $fillable = array('funcao_status');

    public function funcao()
    {
        return $this->belongsTo('Funcao', 'funcao_id_fk', 'funcao_id');
    }

    public function produto()
    {
        return $this->belongsTo('Produto', 'produto_id_fk', 'produto_id');
    }

}

