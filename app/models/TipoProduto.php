<?php

/**
 * Eloquent class to describe the tipo_produto table
 *
 * automatically generated by ModelGenerator.php
 */
class TipoProduto extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'tipo_produto';

    public $primaryKey = 'tipo_id';

    public $timestamps = false;

    protected $fillable = array('tipo_nome', 'tipo_status');

    public function categoria()
    {
        return $this->hasMany('Categoria', 'categoria_idTipo', 'tipo_id');
    }

    public function produto()
    {
        return $this->hasMany('Produto', 'tipo_id_fk', 'tipo_id');
    }

}

