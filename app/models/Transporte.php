<?php

/**
 * Eloquent class to describe the transporte table
 *
 * automatically generated by ModelGenerator.php
 */
class Transporte extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'transporte';

    public $primaryKey = 'transporte_id';

    public $timestamps = false;

    protected $fillable = array('transporte_tipo', 'transporte_desc', 'transporte_valor', 'transporte_status', 'transporte_estado', 'transporte_cidade');

    public function funcionarioTransporte()
    {
        return $this->hasMany('FuncionarioTransporte', 'trasnporte_id_fk', 'transporte_id');
    }

    public function getTransportesCidadeEstado($estado, $cidade)
    {
      return self::whereTransporteEstado($estado)->whereTransporteCidade($cidade)->orderBy('transporte_tipo', 'ASC')->get();
    }

}
