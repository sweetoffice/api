<?php

/**
 * Eloquent class to describe the usuarios_store table
 *
 * automatically generated by ModelGenerator.php
 */
class UsuariosStore extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'usuarios_store';

    public $primaryKey = 'usuario_id';

    public $timestamps = false;

    protected $fillable = array('usuario_nome', 'usuario_email', 'usuario_password', 'remember_token',
        'usuario_status');

    public function cliente()
    {
        return $this->belongsTo('Cliente', 'cliente_id_fk', 'cliente_id');
    }

}

