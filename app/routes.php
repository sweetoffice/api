<?php

Route::get('/','BaseController@inicial');
Route::get('/get','BaseController@teste');
Route::get('/senha','AppController@senha');
Route::get('/tabela/{tabela}', 'BaseController@tabela');
Route::get('/limpar', 'AppController@limpar_tokens');

Route::group(['prefix' => 'pesquisa'], function()
{
    Route::get('/{condition?}', 'SOController@get_database');
});

// Route::get('/generate/models', '\\Jimbolino\\Laravel\\ModelBuilder\\ModelGenerator4@start');
// CRUD
Route::group(['prefix' => 'fornecedor'], function()
{
  Route::get('/', 'SOFornecedorController@get_fornecedores');
  Route::get('/{id}', 'SOFornecedorController@get_fornecedor');
  Route::delete('/{id}', 'SOFornecedorController@delete_fornecedor');
  Route::post('/', 'SOFornecedorController@create_fornecedor');
  Route::put('/{id}', 'SOFornecedorController@update_fornecedor');
});

Route::group(['prefix' => 'cliente'], function()
{
  Route::get('/{id}', 'SOClienteController@get_cliente');
  Route::post('/', 'SOClienteController@create_cliente');
  Route::put('/{id}', 'SOClienteController@update_cliente');
  Route::delete('/{id}', 'SOClienteController@delete_cliente');
});


Route::post('oauth/access_token', function()
{
    return Response::json(Authorizer::issueAccessToken());
});
/*
| Após autenticação
*/
Route::group(['before' => 'oauth'], function()
{
  /*
  | Gerais
  */
	Route::post('/entrar/{cliente}','ApiController@login');
  Route::get('/logout','BaseController@logout');
  /*
  | Models
  */
      /*
      | Banco
      */
      Route::group(['prefix' => 'banco'], function()
      {
        Route::get('/', 'BancoController@get_bancos');
      });
      /*
      | Categoria Produto
      */
      Route::group(['prefix' => 'categoriaproduto'], function()
      {
        Route::get('/{id}', 'CategoriaProdutoController@get_categoriaProduto');
      });
      /*
      | Cidade
      */
      Route::group(['prefix' => 'cidade'], function()
    	{
        Route::get('/{uf}', 'CidadeController@get_cidadesEstados');
      });
      /*
      | Estado
      */
      Route::group(['prefix' => 'estado'], function()
      {
        Route::get('/', 'EstadoController@get_estados');
      });
      /*
      | Empresa
      */
      Route::group(['prefix' => 'empresa'], function()
      {
        Route::get('/', 'EmpresaController@get_empresas');
      });
      /*
      | Medida
      */
      Route::group(['prefix' => 'medida'], function()
    	{
        Route::get('/', 'MedidaController@get_medidas');
      });
      /*
      | Moeda
      */
      Route::group(['prefix' => 'moeda'], function()
    	{
        Route::get('/', 'MoedaController@get_moedas');
      });
      /*
      | Região
      */
      Route::group(['prefix' => 'regiao'], function()
      {
        Route::get('/', 'RegiaoController@get_regiao');
      });
      /*
      | Países
      */
      Route::group(['prefix' => 'paises'], function()
      {
        Route::get('/', 'PaisController@get_paises');
        Route::get('/{id}', 'PaisController@get_pais');
      });
      /*
      | Produtos
      */
      Route::group(['prefix' => 'produto'], function()
      {
        Route::get('/{id}', 'SOProdutoController@get_produto');
      });
      /*
      | Setores
      */
      Route::group(['prefix' => 'setores'], function()
      {
        Route::get('/', 'SetorController@get_setores');
        Route::get('/cargos/{id}','SetorController@get_cargos');
      });
      /*
      | Sindicato
      */
      Route::group(['prefix' => 'sindicato'], function()
      {
        Route::get('/', 'SindicatoController@get_sindicatos');
      });

  /*
  | Sweet Office
  */
	Route::group(['prefix' => 'sweetoffice'], function()
	{
      /*
      | Rotas auxiliativas
      */
			Route::group(['prefix' => 'pesquisar'], function()
			{
					Route::get('/{condition?}', 'SOController@get_database');
			});
      Route::group(['prefix' => 'consultar'], function()
			{
					Route::get('/{tabela}/{coluna?}', 'SOController@get_quantidade');
			});
      /*
      | Banco
      */
      Route::group(['prefix' => 'banco'], function()
      {
        Route::post('/', 'SOBancoController@create_banco');
        Route::put('/{id}', 'SOBancoController@update_banco');
        Route::delete('/{id}', 'SOBancoController@delete_banco');
      });
      /*
      | Bens
      */
      Route::group(['prefix' => 'bens'], function()
      {
          Route::get('/', 'SOBensController@get_bens');
          Route::get('/{id}', 'SOBensController@get_bem');
          Route::delete('/{id}', 'SOBensController@delete_bem');
          Route::post('/', 'SOBensController@create_bem');
          Route::put('/{id}', 'SOBensController@update_bem');
      });
      /*
      | Cargo
      */
      Route::group(['prefix' => 'cargo'], function()
      {
        Route::post('/', 'SOCargoController@create_cargo');
      });
      /*
      | Categoria
      */
      Route::group(['prefix' => 'categoria'], function()
      {
        Route::post('/', 'SOCategoriaController@create_categoria');
        Route::put('/{id}', 'SOCategoriaController@update_categoria');
        Route::delete('/{id}', 'SOCategoriaController@delete_categoria');
      });
      /*
      | Cliente
      */
      Route::group(['prefix' => 'cliente'], function()
      {
        Route::get('/{id}', 'SOClienteController@get_cliente');
        Route::post('/', 'SOClienteController@create_cliente');
        Route::put('/{id}', 'SOClienteController@update_cliente');
        Route::delete('/{id}', 'SOClienteController@delete_cliente');
      });
      /*
      | Clientes
      */
      Route::group(['prefix' => 'clientes'], function()
      {
        Route::get('/', 'SOClienteController@get_clientes');
      });
      /*
      | Cliente Tipo
      */
      Route::group(['prefix' => 'clientetipo'], function()
      {
        Route::get('/', 'ClienteTipoController@get_clienteTipo');
      });
      /*
      | Cliente Grupo
      */
      Route::group(['prefix' => 'clientegrupo'], function()
      {
        Route::get('/', 'ClienteGrupoController@get_clienteGrupo');
      });
      /*
      | Faturamento Tipo
      */
      Route::group(['prefix' => 'faturamentoTipo'], function()
      {
        Route::get('/', 'FaturamentoTipoController@get_faturamentoTipo');
      });
      /*
      | Funcionário
      */
      Route::group(['prefix' => 'funcionario'], function()
      {
        Route::group(['prefix' => 'documento'], function()
        {
            Route::get('/{campo}/{valor}', 'FuncionarioController@get_funcionario_documento');
        });
        Route::get('/{id}', 'FuncionarioController@get_funcionario');
        Route::post('/', 'SOFuncionarioController@create_funcionario');
        Route::put('/{id}', 'SOFuncionarioController@update_funcionario');
        Route::delete('/{id}', 'SOFuncionarioController@delete_funcionario');
      });
      /*
      | Fornecedores
      */
      Route::group(['prefix' => 'fornecedor'], function()
      {
        Route::get('/', 'SOFornecedorController@get_fornecedores');
        Route::get('/{id}', 'SOFornecedorController@get_fornecedor');
        Route::delete('/{id}', 'SOFornecedorController@delete_fornecedor');
        Route::post('/', 'SOFornecedorController@create_fornecedor');
        Route::put('/{id}', 'SOFornecedorController@update_fornecedor');
      });
      /*
      | Empresa
      */
      Route::group(['prefix' => 'empresa'], function()
      {
        Route::post('/', 'SOEmpresaController@create_empresa');
        Route::put('/{id}', 'SOEmpresaController@update_empresa');
        Route::delete('/{id}', 'SOEmpresaController@delete_empresa');
      });
      /*
      | Notificação
      */
      Route::group(['prefix' => 'notificacao'], function()
      {
        Route::get('/{user}/{id}', 'SONotificacoesController@get_notificacao');
      });
      /*
      | Notificações
      */
      Route::group(['prefix' => 'notificacoes'], function()
      {
        Route::get('/{id}', 'SONotificacoesController@get_notificacoes');
        Route::put('/{id}', 'SONotificacoesController@update_notificacoes');
      });
      /*
  		| Produto
  		*/
      Route::group(['prefix' => 'produto'], function()
      {
          Route::post('/', 'SOProdutoController@create_produto');
          Route::put('/{id}', 'SOProdutoController@update_produto');
          Route::delete('/{id}', 'SOProdutoController@delete_produto');
      });
      /*
      | Setor
      */
      Route::group(['prefix' => 'setor'], function()
      {
        Route::post('/', 'SOSetorController@create_setor');
      });
      /*
      | Sindicato
      */
      Route::group(['prefix' => 'sindicato'], function()
      {
        Route::post('/', 'SOSindicatoController@create_sindicato');
      });
      /*
      | Sindicato
      */
      Route::group(['prefix' => 'transporte'], function()
      {
        Route::get('/{estado}/{cidade}', 'TransporteController@get_transportesCidadeEstado');
      });
      /*
  		| Usuário
  		*/
			Route::group(['prefix' => 'usuario'], function()
			{
			    Route::get('/direitos', 'SOUsuarioController@usuario_modulos');
          Route::post('/', 'SOUsuarioController@create_usuario');
          Route::put('/{id}', 'SOUsuarioController@update_usuario');
          Route::delete('/{id}', 'SOUsuarioController@delete_usuario');
			});
	});
  /*
  | Sweet Store
  */
	Route::group(['prefix' => 'sweetstore'], function(){
    /*
    | Jefferson
    */
	});

});
