<?php

/**
 * Eloquent class to describe the cliente table
 *
 * automatically generated by ModelGenerator.php
 */
class Cliente extends Eloquent
{
    protected $table = 'cliente';

    public $primaryKey = 'cliente_id';

    public $timestamps = false;

    protected $fillable = array('cliente_codigo', 'cliente_numero_documento', 'cliente_inscricao_estadual',
        'cliente_uf_sintegra', 'cliente_razao_social', 'cliente_nome_fantasia', 'cliente_cnae', 'regiao_id_fk',
        'cliente_tipo_id_fk', 'cliente_grupo_id_fk', 'faturamento_tipo_id_fk', 'cliente_dia_pagamento', 'cliente_pais',
        'cliente_site', 'cliente_encargos', 'cliente_isento_ipi', 'cliente_isento_icms', 'cliente_isento_pis_confis',
        'cliente_credito_verificado', 'cliente_pre_faturamento', 'cliente_b2b', 'contrato_fornecimento',
        'cliente_venda_consumo', 'cliente_credito_aprovado', 'cliente_nao_cont_icms', 'cliente_desconto_st_ipi',
        'cliente_data_verificacao', 'cliente_desconto', 'cliente_desconto_adicional', 'cliente_tipo_compra',
        'cliente_regime_fiscal', 'cliente_tipo_atividade', 'cliente_nome_cliente', 'cliente_inscricao_suframa',
        'cliente_banco_enviavia_boleto', 'cliente_enviar_protesto', 'cliente_estoque_terceiros', 'cliente_frete',
        'cliente_observacoes', 'banco_id_fk', 'cliente_tipo_dolar', 'cliente_status', 'cliente_tipo_documento');

    public function contapagar()
    {
        return $this->hasMany('Contapagar', 'idCliente', 'cliente_id');
    }

    public function contasreceber()
    {
        return $this->hasMany('Contasreceber', 'idCliente', 'cliente_id');
    }

    public function pedidoVenda()
    {
        return $this->hasMany('PedidoVenda', 'cliente_id_fk', 'cliente_id');
    }

    public function usuariosStore()
    {
        return $this->hasMany('UsuariosStore', 'cliente_id_fk', 'cliente_id');
    }

}

