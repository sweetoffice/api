<?php

/**
 * Eloquent class to describe the contapagar table
 *
 * automatically generated by ModelGenerator.php
 */
class Contapagar extends Eloquent
{
    protected $table = 'contapagar';

    public $primaryKey = 'idcontaPagar';

    public $timestamps = false;

    public function getDates()
    {
        return array('dataEmissao', 'dataDespesa');
    }

    protected $fillable = array('valorTotal', 'dataEmissao', 'dataDespesa', 'numeroPagamento', 'parcelas',
        'observacoes');

    public function cliente()
    {
        return $this->belongsTo('Cliente', 'idCliente', 'cliente_id');
    }

    public function contabilTipo()
    {
        return $this->belongsTo('ContabilTipo', 'idContabilTipo', 'contabil_tipo_id');
    }

    public function tipocusto()
    {
        return $this->belongsTo('Tipocusto', 'idTipoCusto', 'id_tipo_custo');
    }

    public function pagamentoTipo()
    {
        return $this->belongsTo('PagamentoTipo', 'tipoPagamento', 'pagamento_tipo_id');
    }

    public function parcelaspagar()
    {
        return $this->hasMany('Parcelaspagar', 'idConta', 'idcontaPagar');
    }

}

