<?php

/**
 * Eloquent class to describe the faturamento_tipo table
 *
 * automatically generated by ModelGenerator.php
 */
class FaturamentoTipo extends Eloquent
{
    protected $table = 'faturamento_tipo';

    public $primaryKey = 'faturamento_tipo_id';

    public $timestamps = false;

    protected $fillable = array('faturamento_tipo_nome', 'faturamento_tipo_status');

    public function fornecedor()
    {
        return $this->hasMany('Fornecedor', 'faturamento_id_fk', 'faturamento_tipo_id');
    }

    public function pedidoVenda()
    {
        return $this->hasMany('PedidoVenda', 'faturamento_tipo_id_fk', 'faturamento_tipo_id');
    }

}

