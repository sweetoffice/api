<?php

/**
 * Eloquent class to describe the funcionario_endereco table
 *
 * automatically generated by ModelGenerator.php
 */
class FuncionarioEndereco extends Eloquent
{
    protected $table = 'funcionario_endereco';

    public $primaryKey = 'endereco_id';

    public $timestamps = false;

    protected $fillable = array('endereco_rua', 'endereco_numero', 'endereco_complemento', 'endereco_bairro',
        'endereco_cidade', 'endereco_estado', 'endereco_pais', 'endereco_cep');

    public function funcionario()
    {
        return $this->belongsTo('Funcionario', 'funcionario_id_fk', 'funcionario_id');
    }

}

