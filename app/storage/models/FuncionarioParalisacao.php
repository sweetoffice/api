<?php

/**
 * Eloquent class to describe the funcionario_paralisacao table
 *
 * automatically generated by ModelGenerator.php
 */
class FuncionarioParalisacao extends Eloquent
{
    protected $table = 'funcionario_paralisacao';

    public $primaryKey = 'paralisacao_id';

    public $timestamps = false;

    public function getDates()
    {
        return array('paralisacao_inicio', 'paralisacao_termino');
    }

    protected $fillable = array('paralisacao_motivo', 'paralisacao_inicio', 'paralisacao_termino',
        'paralisacao_observacoes', 'paralisacao_status');

    public function funcionario()
    {
        return $this->belongsTo('Funcionario', 'funcionario_id_fk', 'funcionario_id');
    }

}

