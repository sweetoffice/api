<?php

/**
 * Eloquent class to describe the item table
 *
 * automatically generated by ModelGenerator.php
 */
class Item extends Eloquent
{
    protected $table = 'item';

    public $primaryKey = 'iditem';

    public $timestamps = false;

    protected $fillable = array();

}

