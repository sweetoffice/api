<?php

/**
 * Eloquent class to describe the moeda table
 *
 * automatically generated by ModelGenerator.php
 */
class Moeda extends Eloquent
{
    protected $table = 'moeda';

    public $primaryKey = 'moeda_id';

    public $timestamps = false;

    protected $fillable = array('moeda_nome', 'moeda_conversao', 'moeda_status');

    public function bens()
    {
        return $this->hasMany('Bens', 'moeda_id_fk', 'moeda_id');
    }

    public function produto()
    {
        return $this->hasMany('Produto', 'moeda_id_fk', 'moeda_id');
    }

}

