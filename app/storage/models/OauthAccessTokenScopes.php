<?php

/**
 * Eloquent class to describe the oauth_access_token_scopes table
 *
 * automatically generated by ModelGenerator.php
 */
class OauthAccessTokenScopes extends Eloquent
{
    protected $table = 'oauth_access_token_scopes';

    protected $fillable = array();

    public function oauthAccessTokens()
    {
        return $this->belongsTo('OauthAccessTokens', 'access_token_id', 'id');
    }

    public function oauthScopes()
    {
        return $this->belongsTo('OauthScopes', 'scope_id', 'id');
    }

}

