<?php

/**
 * Eloquent class to describe the oauth_auth_code_scopes table
 *
 * automatically generated by ModelGenerator.php
 */
class OauthAuthCodeScopes extends Eloquent
{
    protected $table = 'oauth_auth_code_scopes';

    protected $fillable = array();

    public function oauthAuthCodes()
    {
        return $this->belongsTo('OauthAuthCodes', 'auth_code_id', 'id');
    }

    public function oauthScopes()
    {
        return $this->belongsTo('OauthScopes', 'scope_id', 'id');
    }

}

