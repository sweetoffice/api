<?php

/**
 * Eloquent class to describe the preco_produto table
 *
 * automatically generated by ModelGenerator.php
 */
class PrecoProduto extends Eloquent
{
    protected $table = 'preco_produto';

    public $primaryKey = 'preco_id';

    public $timestamps = false;

    protected $fillable = array('preco_distribuidor', 'preco_markup_distribuidor', 'preco_varejo',
        'preco_markup_varejo', 'preco_final', 'preco_markup_final');

    public function produto()
    {
        return $this->belongsTo('Produto', 'produto_id_fk', 'produto_id');
    }

}

