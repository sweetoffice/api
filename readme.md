## API Projeto

    Framework: Laravel 4.2
    Documentação do Laravel ( http://laravel.com/docs/4.2 )

## Requisitos

    - LAMP ou WAMP
    	- LAMP (https://www.digitalocean.com/community/tutorials/como-instalar-a-pilha-linux-apache-mysql-php-lamp-no-ubuntu-14-04-pt)
    	- PHP (Acima de 5.3)
    	- MySQL (Preferência acima do 5.6)
    	- Apache (O mais recente)

    - Composer (https://getcomposer.org/doc/00-intro.md)

### Observações
    O arquivo ""schema_banco.sql" localizado no diretorio raiz do projeto é o schema padrão

    O arquivo ""bkp_banco.sql" localizado no diretorio raiz do projeto é o backup mais recente do banco.

### Instalação

	No terminal/prompt comando
		" composer install "

		Pergunta: O que é Compsoer?
		Resposta: Gerenciador de pacotes PHP

	Após o composer finalizar, dê um "play" no servidor digitando:
		" php artisan serve "

	Você vai receber uma mensagem verde do tipo:
		" Laravel development server started on http://localhost:8000 ""

	Significa que seu servidor iniciou, acesse no navegador:

		http://localhost:8000

### Regras de Negócio
    VCED
    1111